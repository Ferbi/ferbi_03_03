package com.company;

public class Tablica
{
    void tablica()
    {
        int tablica1[] = new int[20];
        for (int i = 0; i < tablica1.length;i++)
        {
            tablica1[i] = i+1;
        }
        // 1
        System.out.print("Liczby tablicy po kolei - ");
        for (int i = 0; i < tablica1.length;i++)
        {
            System.out.print(tablica1[i] + " ");
        }
        System.out.println();
        // 2
        System.out.print("Liczby tablicy od tylu - ");
        for (int i = tablica1.length-1; i >= 0;i--)
        {
            System.out.print(tablica1[i] + " ");
        }
        System.out.println();
        // 3
        System.out.print("Liczby tablicy co druga  - ");
        for (int i = 0; i < tablica1.length;i+=2)
        {

                System.out.print(tablica1[i] + " ");

        }
        System.out.println();
        // 4
        System.out.print("Liczby tablicy podzielne przez trzy - ");
        for (int i = 0; i < tablica1.length;i++)
        {
            if (tablica1[i]%3==0) {
                System.out.print(tablica1[i] + " ");
            }
        }
        System.out.println();
        // 5
        int suma=0;
        for (int i = 0; i < tablica1.length;i++)
        {
            suma += tablica1[i];

        }
        System.out.println("suma elementow tablicy - "+suma);
        // 6
        suma=0;
        if (tablica1.length >=4)
        {
            for (int i =0;i<4;i++)
            {
              suma+=tablica1[i];
            }
            System.out.println("suma pierwszych 4 elementow = "+suma);}
            else
            {
                System.out.println("za mala tablica");
            }
         // 7
        suma=0;
        if ((tablica1.length >=5) && (tablica1.length-5>=2))
        {
            for (int i = tablica1.length-6;i<tablica1.length;i++)
            {
                suma += tablica1[i];
            }
            System.out.println("suma ostatnich 5 elementow = "+suma);}
        else
        {
            System.out.println("za mala tablica");
        }
        // 8
        suma=0;
        int ile=0;
        for (int i = 0; i < tablica1.length;i++)
        {
            if (suma<=10) {
                suma += tablica1[i];
                ile++;
            }
        }
        ile = ile-1;
        System.out.println("ilosc elementow gdzie suma przekroczyla 10 - "+ile);
    }

}

